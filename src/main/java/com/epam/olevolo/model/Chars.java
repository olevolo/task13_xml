package com.epam.olevolo.model;

public class Chars {

    /** whether site has email address */
    private boolean email;

    /** whether site has news */
    private boolean news;

    /** whether site provides archives to download */
    private boolean archives;

    /** whether site has polls and, if yes, what type */
    private boolean polls;
    private String pollType;

    /** how much costs visiting site */
    private double price;

    public Chars() {}

    public Chars(boolean email, boolean news, boolean archives, boolean polls, String pollType, double price) {
        this.email = email;
        this.news = news;
        this.archives = archives;
        this.polls = polls;
        this.pollType = pollType;
        this.price = price;
    }

    public boolean isEmail() {
        return email;
    }
    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isNews() {
        return news;
    }
    public void setNews(boolean news) {
        this.news = news;
    }

    public boolean isArchives() {
        return archives;
    }
    public void setArchives(boolean archives) {
        this.archives = archives;
    }

    public boolean isPolls() {
        return polls;
    }
    public void setPolls(boolean polls) {
        this.polls = polls;
    }

    public String getPollType() {
        return pollType;
    }
    public void setPollType(String pollType) {
        this.pollType = pollType;
    }

    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Chars{" +
                "email=" + email +
                ", news=" + news +
                ", archives=" + archives +
                ", polls=" + polls +
                ", pollType='" + pollType + '\'' +
                ", price=" + price +
                '}';
    }
}

package com.epam.olevolo.model;

public enum Type {
    NEWS("news"),
    FORUM("forum"),
    MIRROR("mirror");

    private String type;

    Type(String type) {
        this.type = type;
    }
}

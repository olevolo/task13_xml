package com.epam.olevolo.model;

import java.util.List;

public class Site {

    private String title;
    private String type;
    private boolean authorize;
    private List<String> languages;
    private Chars chars;

    public Site() { }

    public Site(String title, String type, boolean authorize, List<String> languages, Chars chars) {
        this.title = title;
        this.type = type;
        this.authorize = authorize;
        this.languages = languages;
        this.chars = chars;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() { return type; }
    public void setType(String type) {
        this.type = type;
    }

    public boolean isAuthorize() {
        return authorize;
    }
    public void setAuthorize(boolean authorize) {
        this.authorize = authorize;
    }

    public List<String> getLanguages() {
        return languages;
    }
    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public Chars getChars() {
        return chars;
    }
    public void setChars(Chars chars) {
        this.chars = chars;
    }

    @Override
    public String toString() {
        return "Site{" +
                "title='" + title + '\'' +
                ", type=" + type.toString() +
                ", authorize=" + authorize +
                ", languages=" + languages +
                ", chars=" + chars +
                '}';
    }
}

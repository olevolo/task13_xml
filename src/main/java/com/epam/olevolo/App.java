package com.epam.olevolo;

import com.epam.olevolo.parser.DOMParser;
import com.epam.olevolo.parser.SAXParser;
import com.epam.olevolo.parser.StAXParser;
import com.epam.olevolo.parser.XMLParser;

import java.io.File;

public class App {

    public static void main(String[] args) {

        File file = new File("sites.xml");

        XMLParser xmlParser = new DOMParser(file);
        xmlParser.parse();
        xmlParser.getInfo();

        xmlParser = new SAXParser(file);
        xmlParser.parse();
        xmlParser.getInfo();

        xmlParser = new StAXParser(file);
        xmlParser.parse();
        xmlParser.getInfo();
    }
}

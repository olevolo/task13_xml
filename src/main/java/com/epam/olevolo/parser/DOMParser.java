package com.epam.olevolo.parser;

import com.epam.olevolo.model.Chars;
import com.epam.olevolo.model.Site;
import com.epam.olevolo.model.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser extends XMLParser {

    private DocumentBuilder builder;
    private Document document;

    public DOMParser(File file) {
        super(file);
    }

    public Document getDocument() {
        return document;
    }

    private void create() {
        DocumentBuilderFactory builderFactory =
                DocumentBuilderFactory.newInstance();
        builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void parse() {
        create();
        try {
            document = builder.parse(new FileInputStream(file.getName()));
        } catch (SAXException|IOException e) {
            e.printStackTrace();
        }
    }

    public void getInfo() {
        sites = getSites();
        System.out.println("DOM:");
        super.getInfo();
    }

    public List<Site> getSites() {

        NodeList nodeList = document.getElementsByTagName("site");
        Node node;

        for (int i = 0; i < nodeList.getLength(); i++) {
            node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                site = new Site();
                site.setTitle(element.getElementsByTagName("title").item(0).getTextContent());
                site.setType(element.getElementsByTagName("type").item(0).getTextContent());
                site.setAuthorize(Boolean.parseBoolean(element.getElementsByTagName("authorize").item(0).getTextContent()));

                languages = getLanguages(element.getElementsByTagName("languages"));
                chars = getChars(element.getElementsByTagName("chars"));

                site.setLanguages(languages);
                site.setChars(chars);

                sites.add(site);
            }
        }
        return sites;
    }

    public List<String> getLanguages(NodeList nodes) {
        List<String> languages = new ArrayList<>();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;
                    languages.add(el.getTextContent());
                }
            }
        }
        return languages;
    }

    public Chars getChars(NodeList nodes) {
        Chars chars = new Chars();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            chars.setEmail(Boolean.parseBoolean(element.getElementsByTagName("email").item(0).getTextContent()));
            chars.setNews(Boolean.parseBoolean(element.getElementsByTagName("news").item(0).getTextContent()));
            chars.setArchives(Boolean.parseBoolean(element.getElementsByTagName("archives").item(0).getTextContent()));
            chars.setPolls(Boolean.parseBoolean(element.getElementsByTagName("polls").item(0).getTextContent()));
            if (chars.isPolls())
                chars.setPollType(element.getElementsByTagName("polls").item(0).getAttributes().item(0).toString());
            else chars.setPollType("no");
            chars.setPrice(Double.parseDouble(element.getElementsByTagName("price").item(0).getTextContent()));
        }
        return chars;
    }
}

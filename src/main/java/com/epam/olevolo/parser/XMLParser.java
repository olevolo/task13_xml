package com.epam.olevolo.parser;

import com.epam.olevolo.model.Chars;
import com.epam.olevolo.model.Site;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class XMLParser {

    protected File file;

    protected List<Site> sites;
    protected Site site;
    protected List<String> languages;
    protected Chars chars;

    public XMLParser(File file) {
        this.file = file;
        this.sites = new ArrayList<>();
    }

    public void getInfo() {
        for (Site site : sites)
            System.out.println(site);
    }

    public abstract void parse();

}

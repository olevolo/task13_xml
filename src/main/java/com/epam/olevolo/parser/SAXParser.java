package com.epam.olevolo.parser;

import com.epam.olevolo.model.Chars;
import com.epam.olevolo.model.Site;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParser extends XMLParser {

    private SAXParserFactory saxParserFactory;
    private SAXHandler saxHandler;

    public SAXParser(File file) {
        super(file);
    }

    @Override
    public void parse() {
        try {
            saxParserFactory = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser saxParser = saxParserFactory.newSAXParser();
            saxHandler = new SAXHandler();
            saxParser.parse(file, saxHandler);
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void getInfo() {
        sites = saxHandler.getSites();
        System.out.println("SAX:");
        super.getInfo();
    }

    class SAXHandler extends DefaultHandler {

        private String curElement = "";

        public List<Site> getSites() {
            return sites;
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes)
                throws SAXException {

            curElement = qName;

            if (curElement.equals("sites")) {
                sites = new ArrayList<>();
            }
            if (curElement.equals("site")) {
                site = new Site();
            }
            if (curElement.equals("languages")) {
                languages = new ArrayList<>();
            }
            if (curElement.equals("chars")) {
                chars = new Chars();
            }
            if (curElement.equalsIgnoreCase("polls")) {
                String pollType = attributes.getValue("type");
                chars.setPollType(pollType);
            }
        }

        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            if (qName.equalsIgnoreCase("languages")) {
                site.setLanguages(languages);
            }
            if (qName.equalsIgnoreCase("chars")) {
                site.setChars(chars);
            }
            if (qName.equalsIgnoreCase("site")) {
                sites.add(site);
            }
        }

        public void characters(char ch[], int start, int length)
                throws SAXException {

            if (curElement.equals("title")) {
                site.setTitle(new String(ch, start, length));
            }
            if (curElement.equals("type")) {
                site.setType(new String(ch, start, length));
            }
            if (curElement.equals("authorize")) {
                site.setAuthorize(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (curElement.equals("language")) {
                languages.add(new String(ch, start, length));
            }
            if (curElement.equals("email")) {
                chars.setEmail(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (curElement.equals("news")) {
                chars.setNews(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (curElement.equals("archives")) {
                chars.setArchives(Boolean.parseBoolean(new String(ch, start, length)));
            }
            if (curElement.equals("polls")) {
                chars.setPolls(Boolean.parseBoolean(new String(ch, start, length)));
                if (!chars.isPolls())
                    chars.setPollType("no");
            }
            if (curElement.equals("price")) {
                chars.setPrice(Double.parseDouble(new String(ch, start, length)));
            }

        }
    }
}

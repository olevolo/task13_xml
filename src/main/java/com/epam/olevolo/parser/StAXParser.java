package com.epam.olevolo.parser;

import com.epam.olevolo.model.Chars;
import com.epam.olevolo.model.Site;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class StAXParser extends XMLParser {

    XMLInputFactory xmlInputFactory;
    XMLEventReader xmlEventReader;
    XMLEvent xmlEvent;

    public StAXParser(File file) {
        super(file);
    }

    private void create() {
        xmlInputFactory = XMLInputFactory.newInstance();
        try {
            xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parse() {
        create();
        try {
            while (xmlEventReader.hasNext()) {
                xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "site":
                            site = new Site();
                            break;
                        case "title":
                            xmlEvent = xmlEventReader.nextEvent();
                            site.setTitle(xmlEvent.asCharacters().getData());
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            site.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "authorize":
                            xmlEvent = xmlEventReader.nextEvent();
                            site.setAuthorize(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "languages":
                            xmlEvent = xmlEventReader.nextEvent();
                            languages = new ArrayList<>();
                            break;
                        case "language":
                            xmlEvent = xmlEventReader.nextEvent();
                            languages.add(xmlEvent.asCharacters().getData());
                            break;
                        case "chars":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars = new Chars();
                            break;
                        case "email":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars.setEmail(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "news":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars.setNews(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "archives":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars.setArchives(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "polls":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars.setPolls(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));

                            Attribute pollsType = startElement.getAttributeByName(new QName("type"));
                            if (chars.isPolls()) {
                                chars.setPollType(pollsType.getValue());
                            }
                            else chars.setPollType("no");
                            break;
                        case "price":
                            xmlEvent = xmlEventReader.nextEvent();
                            chars.setPrice(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("languages")) {
                        site.setLanguages(languages);
                    }
                    if (endElement.getName().getLocalPart().equals("chars")) {
                        site.setChars(chars);
                    }
                    if (endElement.getName().getLocalPart().equals("site")) {
                        sites.add(site);
                    }
                }
            }
        } catch ( XMLStreamException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getInfo() {
        System.out.println("StAX:");
        super.getInfo();
    }

}

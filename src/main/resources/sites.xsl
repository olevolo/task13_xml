<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table{
                    border: 1px ;
                    }

                    td {
                    border: 1px ;
                    background-color: yellow;
                    color: blue;
                    text-align: center;
                    height: 100px;
                    }

                    th {
                    background-color: blue;
                    color: white;
                    width: 250px;
                    }

                </style>
            </head>

            <body>
                <table>
                    <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Authorize</th>
                        <th>Languages</th>
                        <th>Chars</th>
                    </tr>

                    <xsl:for-each select="sites/site">
                        <xsl:sort select="title" />
                        <tr>
                            <td>
                                <xsl:value-of select="title"/>
                            </td>

                            <td>
                                <xsl:value-of select="type" />
                            </td>

                            <td>
                                <xsl:value-of select="authorize" />
                            </td>

                            <td>
                                <xsl:value-of select="languages" />
                            </td>

                            <td>
                                email: <xsl:value-of select="chars/email" />
                                <br/>
                                news: <xsl:value-of select="chars/news" />
                                <br/>
                                archives: <xsl:value-of select="chars/archives" />
                                <br/>
                                polls: <xsl:value-of select="chars/polls" />
                                <xsl:if test="chars/polls = 'true'">
                                    <br/>
                                    polls type:<xsl:value-of select="chars/polls/@type" />
                                </xsl:if>
                                <br/>
                                price: <xsl:value-of select="chars/price" />
                            </td>
                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>